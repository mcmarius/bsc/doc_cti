# Documentația lucrării de licență despre clasificatori de atitudine

Documentația este scrisă în LaTeX, cu pachetul BibLaTeX pentru bibliografie.

Pentru formule, au fost utilizate notațiile din Deep Learning Book, disponibile la adresa https://github.com/goodfeli/dlbook_notation

Pentru prezentare, a fost folosit pachetul Beamer, cu tema [Metropolis](https://github.com/goodfeli/dlbook_notation).
