\documentclass[12pt, a4paper]{article}
\usepackage[top=2.54cm,left=2.54cm,right=2.54cm]{geometry}

\usepackage[utf8]{inputenc}
\usepackage[romanian]{babel}
\title{Rezumatul lucrării de licență\\ Aspecte computaționale ale clasificării textelor foarte scurte după optimism sau pesimism}
\author{Marius Micluța-Câmpeanu}
\date{}

\begin{document}
\maketitle

Lucrarea de licență menționată tratează problema clasificării binare a textelor din perspectiva atitudinii. Aceasta este o problemă similară cu cea a stabilirii polarității, prin analizarea sentimentelor. Totuși, cele două cerințe diferă prin accentul pe care îl pun pe exploatarea informațiilor semantice.

Enunțul problemei este următorul: pentru un text de cel mult 140 de caractere, trebuie determinat dacă acesta exprimă o viziune optimistă sau una pesimistă. De remarcat este faptul că un text lung oferă mult mai multe informații, atât din punct de vedere semantic, cât și sintactic. Dintr-o astfel de perspectivă, textele scurte sunt mai greu de analizat din punct de vedere statistic. Pe de altă parte, există mult mai puțină informație redundantă, iar necesarul de resurse de calcul este redus.

O primă diferență față de un clasificator de sentimente este subliniată în literatura de specialitate. Un asemenea clasificator nu este suficient pentru identificarea corectă a atitudinii, rezultat explicat prin diverse analize lingvistice. Deși cuvintele cu valențe pozitive pot fi buni indicatori pentru mesajele optimiste, nu se poate afirma același lucru despre cuvintele cu valențe negative. Din această cauză, pesimismul este mai dificil de detectat decât optimismul. O altă diferență este aceea că ideea de stabilire a atitudinii este centrată pe individ, pe când aplicațiile analizei de sentimente vizează opiniile oamenilor asupra unor produse. Este evident că nu poate fi aplicat în mod direct un clasificator de sentimente, deoarece și scopurile sunt distincte.

Rezultatele obținute în această lucrare de licență depășesc rezultatele de top raportate în literatură. Setul de date folosit, singurul de altfel conceput pentru rezolvarea acestei probleme, este Optimism Pessimism Twitter (OPT).

Prezentăm în continuare primul clasificator propus, cunoscut în literatură sub numele de BERT. Modelul reprezintă o rețea neurală de tip Transformer, în care textul de intrare este codificat în mod bidirecțional. Parcurgerea textului în ambele direcții contribuie la obținerea unei generalizări mai bune, deoarece rețeaua va învăța să se folosească de contexte din stânga, dar și din dreapta. Antrenarea modelelor BERT se efectuează în două etape: cea de preantrenare, prin învățare nesupervizată, și cea de transfer al cunoștințelor, prin învățare supervizată.

Deoarece rețeaua are foarte mulți parametri, pentru prima etapă sunt necesare multe date de antrenare și un efort de calcul foarte ridicat, prohibitiv în majoritatea cazurilor. Din aceste motive, preantrenarea se efectuează o singură dată, iar rezolvarea problemelor specifice are ca punct de plecare un model preantrenat. Pentru etapa a doua, de transfer al cunoștințelor, este suficient un număr relativ mic de exemple.

Rolul preantrenării este acela de a oferi o generalizare superioară față de alte modele existente. Asftel, indiferent de problema specifică ce trebuie rezolvată, modelul este înzestrat cu înțelegerea mecanismului de funcționare a limbajului natural, asemănător cu raționamentul uman. Prețul plătit pentru această precizie ridicată îl constituie un timp ridicat pentru efectuarea inferenței, de câteva secunde pe sistemul de calcul personal. Cu alte cuvinte, aceasta înseamnă că modelul BERT nu se pretează pentru cazurile în care este necesar un răspuns în timp real, fără alte costuri pentru hardware specializat.

Inevitabil, este firesc să ne întrebăm dacă putem obține rezultate acceptabile pentru probleme relativ simple din punct de vedere tehnic, însă fără modele adânci, cu sute de milioane de parametri. Răspunsul este unul afirmativ, iar modelul propus în lucrarea de licență pentru atingerea acestui scop este bazat pe reprezentările de tip fastText. Aceste reprezentări exploatează structura internă a cuvintelor, cu ajutorul $n$-gramelor la nivel de caracter. Utilizarea așa-ziselor ``subcuvinte'' este esențială pentru interpretarea corectă din punct de vedere semantic a cuvintelor care nu apar în setul de date de antrenare. În ceea ce privește clasificarea în sine, este utilizată o rețea cu un singur strat ascuns, cu ajutorul căreia sunt învățate corespondențe între media reprezentărilor cuvintelor din textul de intrare și eticheta de ieșire. Deși soluția este extrem de simplă, acuratețea modelelor fastText se apropie de cea a modelelor adânci.

Evident, nu trebuie pierdute din vedere diferențele esențiale dintre cele două abordări. BERT este preantrenat pe un model de limbaj, pe hardware dedicat, timp de câteva zile, iar transferul cunoștințelor pe setul de date OPT durează câteva minute. La polul opus, clasificatorul fastText este antrenat de la zero, în cel mult câteva secunde, pe un procesor obișnuit, iar inferența este de ordinul milisecundelor. Mai mult, un model fastText poate fi micșorat printr-un proces de cuantizare, eliminând parametrii care nu contribuie în mod semnificativ la rezultatul clasificării.

Noutatea pe care o promovează lucrarea prezentată este modelul BERT, apărut la sfârșitul anului trecut. Acesta este primul caz de transfer al cunoștințelor aplicat cu succes în prelucrarea limbajului natural, datorită capacității ridicate de generalizare folosind date puține. Totuși, reiterăm faptul că nu trebuie neglijate costurile computaționale ale acestei soluții și propunem o soluție care favorizează viteza în detrimentul performanței.

Am omis din acest rezumat experimentele cu rețelele neurale recurente, întrucât nu au fost obținute rezultate satisfăcătoare. Antrenarea unor astfel de rețele nu este fezabilă în contextul unor resurse limitate, iar optimizarea hiperparametrilor este dificilă.

Raportul dintre acuratețe și viteză este cel care dictează modelul care va fi ales, în funcție de specificul aplicației vizate. Cu toate acestea, cel puțin la nivel teoretic este posibilă comprimarea modelelor adânci, prin procese similare cu cel de cuantizare menționat în cazul fastText. Din nefericire, marile companii nu au interesul să dezvolte modele de top care să consume cât mai puține resurse. Un exemplu este chiar modelul BERT, pentru care nu au fost oferite preantrenate modelele mici atunci când au fost descoperite greșeli de implementare, în urmă cu două luni.

Exploatarea identificării optimismului și a pesimismului în textele scrise este o direcție de cercetare nouă, cu potențial neexplorat. Printre posibilele aplicații care ar putea fi elaborate cu ajutorul soluțiilor propuse în lucrarea descrisă se numără evoluția atitudinii utilizatorilor rețelelor de socializare, descoperirea persoanelor cu depresie și prevenirea sinuciderilor. Dezvoltarea unor astfel de aplicații, cu impact puternic pozitiv asupra evoluției societății, motivează continuarea cercetătilor în această direcție.

\end{document}
