\addtocontents{toc}{\protect\newpage}
\chapter{Rezultate experimentale}
În acest capitol, sunt prezentate contribuțiile autorului la dezvoltarea sistemelor de clasificare a textelor scurte în funcție de optimism sau pesimism. Secțiunea \ref{sec:ds} se ocupă cu studierea setului de date. Secțiunea \ref{sec:lib} enumeră dependențele software ale implementării. Discutarea rezultatelor are loc în secțiunea \ref{sec:results}, iar în secțiunea \ref{sec:cmp} sunt comparate cele mai bune modele antrenate.

\section{Setul de date OPT} \label{sec:ds}
Setul de date Twitter pentru optimism, denumit și OPT \citep{caragea:18}, este primul set de date alcătuit pentru a dezvolta clasificatori pentru identificarea atitudinii. Twitter a fost ales ca sursă de date, datorită faptului că utilizatorii folosesc platforma de socializare pentru a-și exprima gândurile și părerile sau pentru a conversa cu alți utilizatori. Dintr-un total de circa două milioane de mesaje (tweets), 7475 au fost selectate și adnotate de către operatori umani independenți, cu valori între -3 și 3 \citep{ruan:16}.

\subsection{Preprocesarea datelor}
Din fișierul cu mesajele adnotate, sunt eliminate caracterele \texttt{CR}. Toate literele sunt transformate în litere mici. Pentru a putea compara rezultatele proprii cu cele din literatură, a fost preluată din Caragea et al. (2018) \citep{caragea:18} împărțirea datelor în mulțimile de antrenare, validare și test. Am considerat corecte datele prezentate în articole, motiv pentru care mesajele adnotate cu valoarea 0 au fost considerate pozitive (optimiste); în total, sunt 4679 de mesaje optimiste.

\subsection{Analizarea datelor}
Mesajele sunt foarte scurte, de cel mult 150 de caractere. Majoritatea au între 30 și 130 de caractere, dar mai mult de jumătate au minim 70 de caractere. Este dificil de estimat numărul de cuvinte, întrucât mesajele conțin multe greșeli de scriere sau cuvinte alipite.

Există și mesaje care par să nu aibă sens, destinate probabil unei audiențe foarte restrânse, ceea ce introduce zgomot în date. Adnotările au un bias spre optimism, deci clasele nu sunt tocmai echilibrate ($\approx$ 62\% exemple pozitive).

\section{Biblioteci utilizate} \label{sec:lib}
Reprezentările \texttt{fastText}\footnote{\url{https://fasttext.cc}} au proprietatea că media proiecțiilor cuvintelor captează destul de bine semnificația textului și poate fi folosită pentru probleme de clasificare. Cu toate că rezultatele nu sunt de top, cerințele pentru resurse sunt foarte mici, iar antrenarea clasificatorului este extrem de rapidă \citep{joulin:17}. Implementarea \texttt{fastText} este în limbajul \texttt{C++}, versiunea standardului din 2011 \citep{iso:cpp:11}, ceea ce justifică eficiența sporită. Experimentele au fost efectuate cu versiunea 0.2.0 a \texttt{fastText}.
% fastText v0.2.0, scris în C++11 % de adăugat versiunile

Toate celelalte modele au fost implementate în limbajul \texttt{Python} \citep{rossum:95}, versiunea 3.6. \texttt{Anaconda} \citep{anaconda:16}, versiunea 4.5.11, a fost folosit pentru configurarea dependențelor. De asemenea, toate modelele de rețele neurale din această lucrare utilizează intern \texttt{PyTorch} \citep{paszke:17}, versiunea 1.1.0.

\texttt{Flair} \citep{akbik:19}, în prezent versiunea 0.4.2, reprezintă o interfață de nivel foarte înalt peste primitivele de construit rețele neurale din \texttt{PyTorch}. Biblioteca oferă resurse pentru numeroase limbi și promite rezultate de top pentru diverse probleme din prelucrarea limbajului natural: clasificare, recunoașterea entităților, dezambiguizare și etichetarea părților de vorbire.

Implementarea în \texttt{PyTorch} a modelelor BERT \citep{devlin:19}, precum și a altor variante ale arhitecturii Transformer \citep{vaswani:17}, se regăsesc în pachetul \texttt{pytorch-pretrained-bert}\footnote{\url{https://github.com/huggingface/pytorch-pretrained-BERT}}. Versiunea 0.6.2 găzduiește patru astfel de modele.

O altă bibliotecă promițătoare pentru dezvoltarea de aplicații cu componente de învățare profundă este \texttt{fastai}\footnote{\url{https://docs.fast.ai}}. Deși este la versiunea 1.0.52, documentația fie este învechită, fie lipsește, ceea ce îngreunează dezvoltarea.

Pentru preprocesări, am folosit utilitarul \texttt{sed} (4.2.2) și limbajul \texttt{Ruby} \citep{flanagan:08} (2.5.1), datorită productivității și nivelului de confort oferite.

Documentația este redactată în sistemul \LaTeX{}\footnote{\url{https://www.latex-project.org/}}, distribuția TeX Live 2015/Debian\footnote{\url{https://tug.org/texlive/}}. Bibliografia este organizată cu ajutorul BibLaTeX\footnote{\url{https://www.ctan.org/pkg/biblatex}}.

Atât codul, cât și documentația sunt versionate cu programul \texttt{git} (2.7.4) și sunt disponibile pe GitLab\footnote{\url{https://gitlab.com/mcmarius/bsc}}.

Pentru accelerarea antrenării în cazul modelelor \texttt{PyTorch}, a fost folosită o placă video NVIDIA GTX 960M, cu memorie de 4GB și versiunea driver-ului 410.104. Astfel, sunt necesare pachetele CUDA \citep{cuda:07} (10.0.130) și cuDNN \citep{chetlur:14} (7.5.1). Acestea rulează sub sistemul de operare Ubuntu 16.04.6.

\section{Discutarea rezultatelor} \label{sec:results}
Analizarea performanței clasificatorilor este realizată după criteriile considerate în literatură \citep{caragea:18, ruan:16}, respectiv după acuratețe (a se vedea secțiunea \ref{sec:eval} pentru detalii). Din lipsă de resurse materiale, financiare și de timp, rezultatele din literatură nu au putut fi reproduse. A fost făcut tot posibilul pentru ca implementarea propusă în această lucrare să fie cât mai apropiată de cele din literatură, în ceea ce privește preprocesarea datelor, pentru a obține o comparație cât mai relevantă.

\subsection{Clasificare cu fastText}
Clasificatorii \texttt{fastText}, după cum le spune și numele, oferă o soluție rapidă de clasificare. Antrenarea pe CPU pe tot setul de date durează cel mult o secundă, din care cel mai mult durează salvarea modelului pe disc.

Numărul de epoci trebuie să fie mic, deoarece modelul \texttt{fastText}, cu un singur strat ascuns, ajunge să învețe toate exemplele pe dinafară (problemă de \newterm{overfitting}). Au fost folosite între 3 și 5 epoci (5 este valoarea implicită).

Reducerea dimensiunii modelului a fost dusă la extrem, iar vectorii proiecțiilor au dimensiune 16. Lungimea ferestrei de context a fost aleasă în mod empiric 7. Au fost încercate valori între 2 și 5 pentru $n$ în cazul $n$-gramelor la nivel de cuvânt. La $n$-gramele la nivel de caracter, valori între 2 și 6 sunt suficiente. Rata de învățare este 1 și a fost folosit softmax ierarhic, deoarece clasele nu sunt tocmai echilibrate. Pentru a asigura un comportament determinist, clasificatorul a rulat pe un singur fir de execuție.

\begin{table}
\centering
\begin{tabular}{lrrrrrr}
\toprule
Model     & BiLSTM & \textbf{GRUStack} &   CNN &    NB &   SVM & fastText \\
\midrule
Acuratețe &  79.65 &    \textbf{80.19} & 77.78 & 74.20 & 67.80 &    75.63 \\
\bottomrule
\end{tabular}
\caption{Clasificatorul fastText în comparație cu cei de top}
\label{tab:ft}
\end{table}

Capacitatea limitată de reprezentare a modelelor \texttt{fastText} reiese din tabela \ref{tab:ft}. Se observă astfel diferența dintre modelele mici și cele adânci. Din acest motiv, trebuie făcut un compromis între acuratețe și viteză. La modul ideal, după cum susțin și Joulin et al. \citep{joulin:17}, sunt de preferat modelele mici, care aproximează modele mari cu un minim de pierdere din punctul de vedere al performanței. Există mai multe posibilități de îmbunătățire care nu au fost încercate, din lipsă de timp.

\subsection{Rețele neurale recurente}
Pentru aceste experimente, s-a utilizat biblioteca \texttt{Flair}. Modelele oferite de \texttt{Flair} pentru clasificare sunt doar rețele neurale recurente. Ceea ce poate fi modificat este primul strat, al proiecțiilor cuvintelor. Există posibilitatea de a combina mai multe tipuri de proiecții, în speranța de a îmbunătăți performanța.

Totuși, rezultatele obținute nu au fost deloc mulțumitoare, acuratețea fiind sub minimul rezultatelor deja prezentate (65.20\%), motiv pentru care nu vor mai fi incluse. Se confirmă astfel dificultatea de a antrena rețele recurente.

\subsection{Rețele de tip Transformer}
Datorită motivelor de a aplica fine-tuning pe modele de limbaj pentru a rezolva probleme de clasificare, motive expuse în secțiunea \ref{sec:transformer}, am adaptat codul pentru clasificare oferit de \texttt{pytorch-pretrained-bert}\footnote{\raggedright\url{https://github.com/huggingface/pytorch-pretrained-BERT/blob/master/examples/run_classifier.py}}. Preluarea unei implementări de calitate este o procedură standard, adoptată la scară largă.

Mai întâi, setul de date este convertit la formatul \texttt{tsv}. După aceea, este necesară scrierea unei (sub)clase pentru citirea noului set de date. Ca metrică de evaluare este de interes numai acuratețea.

Există două clase de modele BERT disponibile: un model de bază, cu 110 milioane de parametri, și un model uriaș, cu 340 de milioane de parametri. Configurația hardware proprie nu a permis utilizarea modelului uriaș, din cauza cerințelor ridicate de memorie, chiar dacă au fost micșorate la maxim dimensiunea loturilor și a lungimii secvenței maxime de subcuvinte.

Următoarea etapă importantă este stabilirea hiperparametrilor. Numărul de epoci nu trebuie să fie prea mare, din același motiv ca în cazul \texttt{fastText}, cel de a nu face overfit. După experimentarea cu mai multe valori, a fost păstrată valoarea implicită de 3 epoci. Observațiile rămân valabile pentru rata de învățare și pentru proporția cu care este modificată această rată. Rata de învățare este un hiperparametru foarte sensibil, conform literaturii de specialitate \citep{howard:18}, deci sunt de preferat valorile recomandate, întrucât au fost determinate din experiența multor cercetători.

O tokenizare superficială, doar cu ajutorul spațiilor, dezvăluie că cel puțin 1000 de mesaje din setul de antrenare au un minim de 25 de cuvinte, cantitate ce nu trebuie ignorată. O lungime a secvenței maxime de 32 este insuficientă, deoarece preprocesarea specifică modelelor BERT efectuează o tokenizare de subcuvinte și adaugă două token-uri speciale. Astfel, mesajele mai lungi vor fi trunchiate, iar semnificația poate fi afectată considerabil, având în vedere că utilizatorii adaugă la sfârșit cuvinte cheie sub formă de \textit{hashtags}.

Literatura de specialitate recomandă ca valorile hiperparametrilor datelor de intrare să fie puteri ale lui 2, din considerente de eficiență \citep{goodfellow:16}. Cu toate acestea, dacă s-ar fi utilizat secvențe de lungime 64, datele de intrare ar fi conținut multe elemente vide, care nu facilitează învățarea și solicită un consum dublu de memorie. Ca un compromis între valoarea inițială de 32 și cea de 64 recomandată, experimentele următoare au fost făcute cu secvențe de lungime maxim 48. De asemenea, pentru a umple memoria video cât mai mult, exemplele au fost prelucrate în loturi de 24.

Rezultatul experimentelor se regăsește în tabela \ref{tab:bert}. Timpul total de fine-tuning pentru cele 3 epoci este de aproximativ 12 minute, considerabil mai mic decât timpul necesar antrenării de la zero a unor rețele neurale recurente sau convoluționale.

\begin{table}
\centering
\begin{tabular}{lrrrrrr}
\toprule
Model     & BiLSTM & GRUStack &   CNN &    NB &   SVM &  \textbf{BERT} \\
\midrule
Acuratețe &  79.65 &    80.19 & 77.78 & 74.20 & 67.80 & \textbf{82.32} \\
\bottomrule
\end{tabular}
\caption{Clasificatorul bazat pe BERT în comparație cu cei de top}
\label{tab:bert}
\end{table}

\section{Compararea modelelor} \label{sec:cmp}
Pentru a înțelege mai bine diferența dintre cele două modele implementate, funcțiile de evaluare au fost augmentate cu opțiunea de a întoarce toate elementele matricei de confuzie. Pentru modelul \texttt{fastText}, aceste elemente au fost determinate în mod manual, cunoscând etichetele reale, precizia și recall-ul. Pentru modelul \texttt{BERT}, au putut fi utilizate bibliotecile deja incluse (\texttt{sklearn.metrics} \citep{scikit-learn}).

Dat fiind faptul că \texttt{fastText} este un clasificator relativ simplu, nu ar trebui să fie deloc surprinzător bias-ul pentru clasificarea pozitivă a 72\% dintre exemple. Motivul a fost precizat anterior: clasele din setul de antrenare nu sunt echilibrate, ceea ce determină clasificatorul să încerce să ``profite'' în acest sens. Trebuie menționat că utilizarea funcției de cost softmax ierarhic aduce îmbunătățiri.

Modelul \texttt{BERT} se remarcă printr-un număr mult mai mic de exemple clasificate greșit, $\approx$ 7\%, atât în ceea ce privește exemplele optimiste, cât și cele pesimiste. Acest fapt dovedește o excelentă capacitate de generalizare.

Este interesant de observat faptul că exemplele cu adevărat pozitive din setul de test au fost clasificate aproape identic de către cei doi clasificatori. Investigații amănunțite sunt necesare pentru a stabili dacă este vorba despre \textit{aceleași} exemple pozitive clasificate corect sau greșit. Indiferent de situație, o posibilă explicație este aceea că ambele modele extrag caracteristici asemănătoare pentru identificarea exemplelor pozitive, în ciuda faptului că funcțiile învățate sunt complet diferite.

Ipoteza din paragraful anterior este susținută de rezultatele din Caragea et al. \citep{caragea:18} asupra analizei polarității mesajelor. Din acest motiv, mesajele optimiste conțin adesea cuvinte cu conotații pozitive, ceea ce le face mult mai ușor de clasificat decât cele negative.

O altă explicație a rezultatelor obținute este motivată de natura datelor. Mesajele care au un scor foarte apropiat de 0 sunt considerate neutre de Ruan et al. \citep{ruan:16}. Astfel, ambii clasificatori exprimă mai degrabă aceleași preferințe pentru clasificarea exemplelor neutre.

Cele două presupuneri nu se exclud și nu sunt nici exhaustive. Nu trebuie pierdute din vedere diferențele radicale dintre cele două arhitecturi. De aceea, experimente suplimentare sunt necesare pentru a putea ajunge la o concluzie referitoare la acest comportament al învățării.

\begin{table}
\centering
% please refer to https://en.wikibooks.org/wiki/LaTeX/Tables#Spanning_in_both_directions_simultaneously
\begin{tabular}{ r|c|c| }
\multicolumn{1}{r}{}
 &  \multicolumn{1}{c}{pozitiv real}
 & \multicolumn{1}{c}{negativ real} \\
\cline{2-3}
pozitiv sistem & TP: 409 & FP: 129 \\
\cline{2-3}
negativ sistem & FN: 53 & TN: 156 \\
\cline{2-3}
\end{tabular}
\caption{Matricea de confuzie pentru clasificatorul fastText}
\label{tab:conf:mat:ft}
\end{table}
% :tp=>409, :tn=>156, :fp=>129, :fn=>53

\begin{table}
\centering
% please refer to https://en.wikibooks.org/wiki/LaTeX/Tables#Spanning_in_both_directions_simultaneously
\begin{tabular}{ r|c|c| }
\multicolumn{1}{r}{}
 &  \multicolumn{1}{c}{pozitiv real}
 & \multicolumn{1}{c}{negativ real} \\
\cline{2-3}
pozitiv sistem & TP: 408 & FP: 78 \\
\cline{2-3}
negativ sistem & FN: 54 & TN: 207 \\
\cline{2-3}
\end{tabular}
\caption{Matricea de confuzie pentru clasificatorul BERT}
\label{tab:conf:mat:bert}
\end{table}
% 'tp': 408, 'tn': 207, 'fp': 78, 'fn': 54
