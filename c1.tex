\chapter{Motivație}
Acest capitol descrie contextul în care a apărut domeniul de clasificare a textelor scurte (tweets) în categoriile optimism/pesimism, precum și argumente în favoarea dezvoltării de aplicații în acest domeniu. Secțiunea \ref{sec:sent} definește problema analizării opiniilor și a sentimentelor. Secțiunea \ref{sec:opt} studiază identificarea optimismului și a pesimismului, o nouă ramură a prelucrării limbajului. Secțiunea \ref{sec:rw} prezintă principalele articole din literatura de specialitate.

\section{Identificarea sentimentelor} \label{sec:sent}
Cercetarea în domeniul identificării sentimentelor și a opiniilor se datorează, în mare parte, dezvoltării rețelelor de socializare și a altor modalități de comunicare pe Internet. Fără existența unor cantități considerabile de păreri în format digital, cercetarea în acest domeniu nu ar fi fost posibilă \citep{liu:15}.

Un impact major al Internetului îl constituie eliminarea barierelor geografice (până la un punct) și crearea unor noi mijloace de interacțiune, ceea ce a facilitat exprimarea de viziuni, lansarea de discuții și formarea de comunități. Pe lângă acest aspect, opiniile sunt un element central în procesul de luare a deciziilor, datorat, pe de o parte, naturii colaborative a societății, iar, pe de altă parte, faptului că oamenii consideră prețioasă experiența celorlalți în domeniile în care se iau acele decizii \citep{liu:15}.

\subsection{Descriere generală}
\newterm{Analizarea sentimentelor} (sentiment analysis), denumită uneori \newterm{extragerea opiniilor} (opinion mining), reprezintă o direcție de cercetare aflată în continuă dezvoltare, care studiază opiniile, sentimentele, emoțiile și atitudinile oamenilor asupra diverselor entități și asupra atributelor acestor entități, exprimate în formă scrisă, pentru a determina polaritatea și intensitatea lor \citep{liu:15}.

În literatură, sunt definite mai multe niveluri de granularitate la care se efectuează analizarea sentimentelor: la nivel de \newterm{document}, de \newterm{propoziție} și de \newterm{aspect} \citep{liu:15}.

Primul nivel presupune că documentul exprimă, în ansamblul său, o singură opinie, referitoare la o singură entitate. La următorul nivel, pentru fiecare propoziție, se determină dacă aceasta exprimă vreo părere. Extragerea opiniilor la nivel de aspect urmărește să descopere cu exactitate entitatea la care o anumită opinie face referire. Acest ultim nivel este exploatat cel mai adesea în aplicațiile practice \citep{liu:15}.

Modul cel mai evident și mai firesc de a identifica opiniile pare să fie realizabil cu ajutorul unor indicatori de sentimente, compilate în ceea ce se cheamă \newterm{lexic de sentimente}. Importanța acestor dicționare de cuvinte nu trebuie supraestimată, din cauza conotațiilor opuse pe care le au unii termeni, în funcție de context. O altă situație nefavorabilă este cea în care nu este exprimată, de fapt, nicio părere. Deoarece oamenii folosesc limbajul în moduri inovative, de exemplu, în remarci sarcastice, opiniile devin și mai greu de prezis doar prin simple euristici \citep{liu:15}.

În ceea ce privește natura conținutului, textul produs de utilizatori pe rețelele de socializare se împarte în două categorii: texte de sine stătătoare\footnote{Corespunzător termenului din engleză \textit{post}. Cu toate că, în limba română, este frecvent folosită varianta \textit{postare}, termenul nu a intrat oficial în dicționar.} și \newterm{dialoguri}. Aceste două categorii sunt complementare. Cele din urmă, sub formă de discuții sau dezbateri, prezintă un deosebit interes pentru cercetători, întrucât se pretează analizei interacțiunii dintre participanți. De asemenea, există potențial pentru a explora rolul fiecărui participant, modul său de argumentare, dar și colaborarea dintre participanți întru susținerea uneia dintre părți \citep{liu:15}.

Anonimitatea oferită de Internet (cu excepțiile de rigoare) este o sabie cu două tăișuri. Pe de o parte, utilizatorii consideră că își pot exprima liber și fără consecințe orice fel de opinii, deci oferă feedback valoros celor din jur. Pe de altă parte, actorii malițioși au egala posibilitate de a acționa din umbră, pentru a răspândi opinii false, pentru a promova produse sau pentru a denigra persoane publice. Scopul extragerii opiniilor, în acest caz, este de a \newterm{detecta opiniile fabricate} \citep{liu:15}.

\subsection{Aplicații}
Ingredientul principal din cadrul rețelelor de socializare este însăși transmiterea părerilor. De aceea, în mod inevitabil, % și fără cale de întoarcere
analizarea sentimentelor % este o piatră de temelie
este indispensabilă unei bune funcționări a acestor rețele. Dintr-o perspectivă axată complet pe ideea că opiniile sunt elementul central, conținutul din mediile de socializare înseamnă subiectele cele mai discutate, împreună cu dezbaterile pe marginea respectivelor subiecte. Mai mult, despre evenimentele pe care majoritatea le ignoră, se poate afirma că nu sunt relevante, potrivit motivului că nu stârnesc reacții \citep{liu:15}.

În egală măsură, mediile de socializare facilitează și explorarea comportamentului utilizatorilor. Aceștia pot fi descriși și identificați după sentimentele și părerile pe care le exteriorizează. Profilul astfel creat reflectă preferințele și personalitatea utilizatorilor. Informațiile se pot utiliza mai departe pentru recomandare de produse și servicii. Suplimentar, mediile de socializare permit aflarea opiniei publice \citep{liu:15}.

Printre aplicațiile de succes se numără predicția valorilor bursei sau a candidaților politici în perioada alegerilor, pe baza intensității sentimentelor mesajelor din mediile de socializare, previziuni asupra succesului comercial al filmelor, în funcție de recenzii preliminare, și agregarea viziunilor angajaților în interiorul companiilor. Așadar, domeniul se remarcă printr-o mare varietate de aplicații \citep{liu:15}.

\section{Identificarea optimismului și a pesimismului} \label{sec:opt}
După cum a fost menționat în introducere, modul în care este folosit limbajul de către om reprezintă un puternic identificator la nivel de individ. Unele studii din psihologie arată că, de-a lungul timpului, există o stabilitate consistentă în modul de exprimare în scris, cu toate că subiectele abordate de participanții la experimente sunt foarte diverse. Aceste trăsături discriminatorii nu scot însă la suprafață într-un mod suficient de clar legăturile dintre stilul de exprimare și starea de spirit \citep{pennebaker:99}.

Testul de orientare în viață (Life Orientation Test sau \newterm{LOT}) \citep{scheier:85} este o metodă din psihologie care determină nivelul de optimism al unei persoane. LOT este definit pe baza așteptărilor unui individ referitoare la felul în care acesta consideră că se desfășoară lucrurile la modul general. Totuși, orice astfel de test are dezavantajul unor costuri materiale și de timp ridicate în cazul aplicării la scară largă. Acesta este contextul în care se pune problema identificării optimismului și a pesimismului în mod automat, transpusă ca problemă de clasificare \citep{ruan:16}.

\subsection{Diferențe față de identificarea sentimentelor}
O abordare superficială ar putea considera suficientă utilizarea unui clasificator deja antrenat pentru analizarea sentimentelor, plecând de la intuiția că optimismul este asociat cu sentimente pozitive, iar pesimismul este asociat cu sentimente negative. Caragea et al. (2018) \citep{caragea:18} probează această ipoteză simplistă și nu obțin rezultate favorabile, ceea ce îi determină să antreneze clasificatori specializați \citep{caragea:18}.

O explicație pentru diferența dintre cele două probleme este diferența de nivel de detaliu. Pentru identificarea sentimentelor, cuvintele dintr-un lexic de sentimente oferă indicii puternice într-o anumită direcție, iar entitatea care constituie ținta sentimentelor are o importanță de moment în viața individului. Dimpotrivă, în psihologie se afirmă că atitudinea unui individ prezintă numai variații minore de-a lungul timpului sau rămâne constantă \citep{scheier:85}. Astfel, identificarea atitudinii este o chestiune mult mai subtilă, întrucât viziunea despre viață nu reiese la fel de clar din textul scris. Ca o concluzie, spre deosebire de exprimarea sentimentelor, atitudinea reflectă modul de viață.

\subsection{Aplicații}
Identificarea atitudinii are potențial de aplicabilitate în numeroase domenii. Prin aplicarea la scară largă, clasificarea atitudinii ar permite studierea amănunțită a comportamentului online al utilizatorilor pe platformele de socializare, raportat la personalitatea identificată de clasificator \citep{ruan:16}. Ruan et al. (2016) propun includerea acestor informații în sistemele de recomandare, pentru a îmbunătăți sugestiile de persoane pe care un utilizator ar dori să le ``urmărească''\footnote{Conotația este pozitivă. Termenul din engleză este \textit{follow}.}. Abordarea prin care utilizatorii comunică doar cu persoane cu preferințe similare prezintă pericolul de a transforma această comunicare în camere cu ecou \citep{barbera:15}.

Caragea et al. (2018) identifică alte perspective de aplicare a problemei enunțate. Determinarea persoanelor pesimiste ar putea ajuta la descoperirea depresiei și la prevenirea sinuciderilor \citep{caragea:18}, dat fiind că pesimiștii tind să comunice mai mult \citep{ruan:16}.

\section{Lucrări în domeniu} \label{sec:rw}
În încheierea acestui capitol, sunt trecute în revistă o infimă parte dintre articolele care au definit și au influențat cele două ramuri ale prelucrării limbajului prezentate în secțiunile precedente. Datorită lor, extragerea opiniilor a cunoscut o evoluție rapidă și constantă, în prezent fiind integrată în nenumărate aplicații \citep{liu:15}.

\subsection{Clasificarea sentimentelor}
De la începutul anilor 2000, extragerea opiniilor este una dintre cele mai active direcții de cercetare din cadrul prelucrării limbajului natural \citep{zhang:17}. Deoarece a coincis cu începuturile comerțului electronic, nu este de mirare că atenția cercetătorilor s-a îndreptat spre analizarea și rezumarea recenziilor pentru produse comerciale \citep{hu:04}.

Popularitatea studierii opiniilor în anii următori a condus la dezvoltarea acestor sisteme pentru situații foarte diferite. Blitzer et al. (2007) dezvoltă modalități de transfer al cunoștințelor între domenii, cu scopul de a rezolva dependența de domeniu a clasificatorilor antrenați în mod supervizat \citep{blitzer:07}.

Una dintre primele, dar și cele mai de succes ramuri ale procesării limbajului este traducerea automată, datorită naturii multiculturale a spațiului digital. Prelucrarea sentimentelor în mai multe limbi a fost studiată de Mihalcea et al. (2007) pentru identificarea și studierea subiectivismului \citep{mihalcea:07}.

De la reguli de clasificare construite manual, se trece treptat și în acest domeniu la metode moderne. Socher et al. (2013) realizează clasificarea sentimentelor la nivel de propoziție, cu ajutorul rețelelor neurale recursive adânci, pentru a exploata compunerea semantică a constituenților. De asemenea, ei propun un nou corpus, Stanford Sentiment Treebank (SST), cu arbori de derivare adnotați complet \citep{socher:13}.

Xu et al. (2019) studiază transferul cunoștințelor bazat pe învățarea profundă cu popularul model BERT. Printre co-autori se numără și Bing Liu\footnote{Bing Liu este considerat în literatură ``părintele'' extragerii opiniilor.} \citep{xu:19}. Importanța colaborării și a subiectului este dată de faptul că Bing Liu nu recomanda sistemele bazate pe învățarea automată, din cauza dificultăților de interpretare a modului de obținere al rezultatelor, în contrast cu sistemele care folosesc reguli manuale \citep{liu:15}.

\subsection{Clasificarea atitudinii}
Întrucât această direcție de cercetare este foarte nouă, există foarte puține lucrări în domeniu. Din investigațiile autorului, au fost identificate numai două articole pe acest subiect. Ruan et al. (2016) \citep{ruan:16} introduc primul corpus adnotat pentru optimism, OPT (Optimism/Pessimism Twitter), și stabilesc primele rezultate.

Caragea et al. (2018) \citep{caragea:18} explorează utilizarea învățării profunde pentru clasificarea textelor din corpusul OPT după optimism și pesimism. Ei arată că un clasificator de sentimente nu poate fi aplicat direct și pentru determinarea atitudinii. De asemenea, ei realizează o analiză lingvistică asupra timpului verbelor și a polarității cuvintelor.
